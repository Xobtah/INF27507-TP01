class CreateEnfants < ActiveRecord::Migration[5.1]
  def change
    create_table :enfants do |t|
      t.references :client, foreign_key: true
      t.string :Nom
      t.string :Prenom
      t.datetime :DateNaissance

      t.timestamps
    end
  end
end
