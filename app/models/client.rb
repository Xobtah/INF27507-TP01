class Client < ApplicationRecord
    has_many :enfants, dependent: :destroy
    accepts_nested_attributes_for :enfants, allow_destroy: true
end
