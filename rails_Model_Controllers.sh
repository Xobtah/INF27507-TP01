rails generate scaffold Client Prenom:string Nom:string \
DateNaissance:datetime NAS:string Adresse_id:integer \
NombreEnfants:integer CompteTaxesProprietaire:integer;

rails generate model Client_has_Conjoint Client_id:integer \
Client_conjoint_id:integer DateDebut:datetime DateFin:datetime;

rails generate model Client_has_Enfant Enfant_id:integer Client_id:integer \
Lien:integer;

rails generate scaffold Enfant Nom:string Prenom:string \
DateNaissance:datetime;

rails generate scaffold Etude SecteurEtude:string Niveau:string \
DateDebut:datetime DateComplition:datetime Client_id:integer \
Institution_id:integer;

rails generate scaffold Institution Nom:string Adresse_id:integer;

rails generate scaffold Adresse NumeroCivique:string Rue:string \
CodePostal:string Ville:string Province:string;

rails generate model Client_has_Employeur Client_id:integer Employeur_id:integer \
DateDebut:datetime DateFin:datetime;

rails generate scaffold Employeur nom:string Adresse_id:integer;

rails generate scaffold Client_EtatCivil Client_id:integer EtatCivil_id:integer \
DateDebut:datetime DateFin:datetime;

rails generate scaffold EtatCivil type:string;

rake db:migrate
